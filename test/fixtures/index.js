module.exports = {
  dbconfig: {
    username: 'fakeymcfake',
    email: 'fakey_mcfake@pseudonymous.com',
    password: 'correct battery horse staple',
    collection: 'super secret scintillatingly scandalous documents'
  },
  regularuser: {
    username: 'fakeymcnormal',
    email: 'fakey_mcfakerer@pseudonymous.com',
    password: 'incorrect solar zebra paperclip',
    admin: false
  },
  adminuser: {
    username: 'fakeymcadmin',
    email: 'fakey_mcfakererer@pseudonymous.com',
    password: 'pa55w0rd',
    admin: true
  }
}
